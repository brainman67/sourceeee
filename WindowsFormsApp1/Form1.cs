﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
	public partial class Form1 : Form
	{
		public static int sq;

		public Form1()
		{
			InitializeComponent();			
		}

		private void JobBtn_Click(object sender, EventArgs e)
		{
			MyChart.Series[0].Points.Clear();
			MyChart.Series[1].Points.Clear();
			double a, b, h;
			double.TryParse(Atbx.Text, out a);
			double.TryParse(Btbx.Text, out b);
			double.TryParse(Htbx.Text, out h);
			int.TryParse(SQtbx.Text, out sq);

			var StartList = new List<double>();

			for (double i = a; i <= b; i += h)
			{
				StartList.Add(i);
			}

			StartList = StartList.OrderBy(i => Guid.NewGuid()).ToList();

			for (int i = 0; i < StartList.Count; i++)
			{
				MyChart.Series[1].Points.AddXY(i, StartList[i]);
			}

			var AproxArray = GetApproxyArray(StartList);

			for (int i = 0; i < AproxArray.Count; i++)
			{
				double x = (double)i / AproxArray.Count * (b - a) + a;
				MyChart.Series[0].Points.AddXY(Math.Round(x, 2) - 1, AproxArray[i]);
			}
		}

		public List<double> GetApproxyArray(List<double> StartList)
		{
			if (S20.Checked)
			{
				return M.Equation(StartList, S.MyMethodS20);
			}
			if (S21.Checked)
			{
				return M.Equation(StartList, S.MyMethodS21);
			}
			if (S22.Checked)
			{
				return M.Equation(StartList, S.MyMethodS22);
			}
			if (S30.Checked)
			{
				return M.Equation(StartList, S.MyMethodS30, true);
			}
			if (S31.Checked)
			{
				return M.Equation(StartList, S.MyMethodS31);
			}
			if (S32.Checked)
			{
				return M.Equation(StartList, S.MyMethodS32);
			}
			if (S40.Checked)
			{
				return M.Equation(StartList, S.MyMethodS40);
			}
			if (S41.Checked)
			{
				return M.Equation(StartList, S.MyMethodS41, true);
			}
			if (S42.Checked)
			{
				return M.Equation(StartList, S.MyMethodS42);
			}
			if (S50.Checked)
			{
				return M.Equation(StartList, S.MyMethodS50, true);
			}

			return M.Equation(StartList, S.MyMethodS20);
		}

		//public List<double> Equation(List<double> Pi, int Sq)
		//{
		//	var res = new List<double>();
		//	var _x = new List<double>();

		//	var h = Math.Round(2d / Sq, 2);
		//	for (double i = -1; i < 1.00000000001; i += h)
		//	{
		//		_x.Add(Math.Round(i, 2));
		//	}

		//	for (int i = 1; i < Pi.Count - 2; i++)
		//	{
		//		for (int r = 0; r < _x.Count; r++)
		//		{
		//			//res.Add(MyMethod(Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], _x[r]));
		//		}
		//	}
		//	return res;
		//}
	}
}
