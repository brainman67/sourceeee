﻿using System;

namespace WindowsFormsApp1
{
	public static class S
	{
		public static double MyMethodS20(double pi_1, double pi, double pi__1, double x)
		{
			var res = (1 / 8d) * (
				(1 - x) * (1 - x) * pi_1 +
				(6 - 2 * x * x) * pi +
				(1 + x) * (1 + x) * pi__1);
			return Math.Round(res, 2);
		}

		public static double MyMethodS21(double pi_1, double pi, double pi__1, double pi__2, double x)
		{
			var res = (1 / 48d) * (
				(1 - x) * (1 - x) * (1 - x) * pi_1 +
				(23 - 15 * x - 3 * x * x + 3 * x * x * x) * pi +
				(23 + 15 * x - 3 * x * x - 3 * x * x * x) * pi__1 +
				(1 + x) * (1 + x) * (1 + x) * pi__2);
			return Math.Round(res, 2);
		}

		public static double MyMethodS22(double pi_3, double pi_2, double pi_1, double pi, double pi__1, double pi__2, double pi__3, double x)
		{
			var res = (1 / 288d) * (
				(1 - x) * (1 - x) * pi_3 +
				(-4 + 20 * x - 12 * x * x) * pi_2 +
				(-5 - 106 * x + 75 * x * x) * pi_1 +
				(304 - 128 * x * x) * pi +
				(-5 + 106 * x + 75 * x * x) * pi__1 +
				(-4 - 20 * x - 12 * x * x) * pi__2 +
				(1 + x) * (1 + x) * pi__3);
			return Math.Round(res, 2);
		}

		public static double MyMethodS30(double pi_1, double pi, double pi__1, double pi__2, double x)
		{
			var res = (1 / 48d) * (
				(1 - x) * (1 - x) * (1 - x) * pi_1 +
				(23 - 15 * x - 3 * x * x + 3 * x * x * x) * pi +
				(23 + 15 * x - 3 * x * x - 3 * x * x * x) * pi__1 +
				(1 + x) * (1 + x) * (1 + x) * pi__2);
			return Math.Round(res, 2);
		}

		public static double MyMethodS31(double pi_2, double pi_1, double pi, double pi__1, double pi__2, double pi__3, double x)
		{
			var res = (1 / 1152d) * (
				-5 * (1 - x) * (1 - x) * (1 - x) * pi_2 +
				(-81 - 27 * x + 117 * x * x - 49 * x * x * x) * pi_1 +
				(662 - 570 * x - 102 * x * x + 122 * x * x * x) * pi +
				(662 + 570 * x - 102 * x * x - 122 * x * x * x) * pi__1 +
				(-81 + 27 * x + 117 * x * x + 49 * x * x * x) * pi__2 +
				-5 * (1 + x) * (1 + x) * (1 + x) * pi__3
				);
			return Math.Round(res, 2);
		}

		public static double MyMethodS32(double pi_3, double pi_2, double pi_1, double pi, double pi__1, double pi__2, double pi__3, double pi__4, double x)
		{
			var res = (1 / 55296d) * (
				47 * (1 - x) * (1 - x) * (1 - x) * pi_3 +
				(653 + 579 * x - 1425 * x * x + 569 * x * x * x) * pi_2 +
				(-6849 + 1383 * x + 6885 * x * x - 3339 * x * x * x) * pi_1 +
				(33797 - 33705 * x - 5601 * x * x + 7501 * x * x * x) * pi +
				(33797 + 33705 * x - 5601 * x * x - 7501 * x * x * x) * pi__1 +
				(-6849 - 1383 * x + 6885 * x * x + 3339 * x * x * x) * pi__2 +
				(653 - 579 * x - 1425 * x * x - 569 * x * x * x) * pi__3 +
				47 * (1 + x) * (1 + x) * (1 + x) * pi__4
				);
			return Math.Round(res, 2);
		}

		public static double MyMethodS40(double pi_2, double pi_1, double pi, double pi__1, double pi__2, double x)
		{
			var res = (1 / 384d) * (
				(1 - x) * (1 - x) * (1 - x) * (1 - x) * pi_2 +
				(76 - 88 * x + 24 * x * x + 8 * x * x * x - 4 * x * x * x * x) * pi_1 +
				(230 - 60 * x * x + 60 * x * x * x * x) * pi +
				(76 + 88 * x + 24 * x * x - 8 * x * x * x - 4 * x * x * x * x) * pi__1 +
				(1 + x) * (1 + x) * (1 + x) * (1 + x) * pi__2);

			return Math.Round(res, 2);
		}

		public static double MyMethodS41(double pi_3, double pi_2, double pi_1, double pi, double pi__1, double pi__2, double pi__3, double x)
		{
			var res = (1 / 1536d) * (
				-1 * (1 - x) * (1 - x) * (1 - x) * (1 - x) * pi_3 +
				(-70 + 64 * x + 12 * x * x - 32 * x * x * x + 10 * x * x * x * x) * pi_2 +
				(225 - 524 * x + 198 * x * x + 52 * x * x * x - 31 * x * x * x * x) * pi_1 +
				(1228 - 408 * x * x + 44 * x * x * x * x) * pi +
				(225 + 524 * x + 198 * x * x - 52 * x * x * x - 31 * x * x * x * x) * pi__1 +
				(-70 - 64 * x + 12 * x * x + 32 * x * x * x + 10 * x * x * x * x) * pi__2 +
				-1 * (1 + x) * (1 + x) * (1 + x) * (1 + x) * pi__3);
			return Math.Round(res, 2);
		}

		public static double MyMethodS42(double pi_4, double pi_3, double pi_2, double pi_1, double pi, double pi__1, double pi__2, double pi__3, double pi__4, double x)
		{
			var res = (1 / 92160d) * (
					13 * (1 - x) * (1 - x) * (1 - x) * (1 - x) * pi_4 +
					(876 - 696 * x - 360 * x * x + 552 * x * x * x - 164 * x * x * x * x) * pi_3 +
					(-5084 + 8104 * x - 840 * x * x - 2648 * x * x * x + 964 * x * x * x * x) * pi_2 +
					(8404 - 36952 * x + 16872 * x * x + 3848 * x * x * x - 2588 * x * x * x * x) * pi_1 +
					(83742 - 31500 * x * x + 3550 * x * x * x * x) * pi +
					(8404 + 36952 * x + 16872 * x * x - 3848 * x * x * x - 2588 * x * x * x * x) * pi__1 +
					(-5084 - 8104 * x - 840 * x * x + 2648 * x * x * x + 964 * x * x * x * x) * pi__2 +
					(876 + 696 * x - 360 * x * x - 552 * x * x * x - 164 * x * x * x * x) * pi__3 +
					13 * (1 + x) * (1 + x) * (1 + x) * (1 + x) * pi__4);
			return Math.Round(res, 2);
		}

		public static double MyMethodS50(double pi_2, double pi_1, double pi, double pi__1, double pi__2, double pi__3, double x)
		{
			var res = (1 / 3840d) * (
				(1 - x) * (1 - x) * (1 - x) * (1 - x) * (1 - x) * pi_2 +
				(237 - 375 * x + 210 * x * x - 30 * x * x * x - 15 * x * x * x * x + 5 * x * x * x * x * x) * pi_1 +
				(1682 - 770 * x - 220 * x * x + 140 * x * x * x + 10 * x * x * x * x - 10 * x * x * x * x * x) * pi +
				(1682 + 770 * x - 220 * x * x - 140 * x * x * x + 10 * x * x * x * x + 10 * x * x * x * x * x) * pi__1 +
				(237 + 375 * x + 210 * x * x + 30 * x * x * x - 15 * x * x * x * x - 5 * x * x * x * x * x) * pi__2 +
				(1 + x) * (1 + x) * (1 + x) * (1 + x) * (1 + x) * pi__3);
			return Math.Round(res, 2);
		}
	}
}